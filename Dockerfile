ARG VERSION=2021.2.3

FROM homeassistant/home-assistant:$VERSION

RUN mkdir -p /data/recordings && \
    mkdir -p /data/webdata